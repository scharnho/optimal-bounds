# Optimal Bounds

Code for the paper "Robust Uncertainty Bounds in Reproducing Kernel Hilbert Spaces:  A Convex Optimization Approach".

Authors: P. Scharnhorst, E. T. Maddalena, Y. Jiang and C. N. Jones.

## Experiments

To replicate the experiments of the paper, see the file `experiments_paper.py`. The parameter settings
- `exptype = "3d"`
- `evalpoints_sqrt = 50`

together with `eps = 1` and `eps = 5` produce the plots of the optimal bound comparison. The settings

- `exptype = "2d"`
- `evalpoints_sqrt = 100`
- `slice_index = 10`
- `eps = 1`
- `lambda_0 = 0.001`

together with `approx_steps = 5` and `approx_steps = 10` produce the results of the slice comparison, although in a slightly different layout (max. 2 bounds in one plot).

