% Supplementary material for the paper:
% Robust Uncertainty Bounds in Reproducing Kernel Hilbert Spaces:  
% A Convex Optimization Approach'
% Authors: P. Scharnhorst, E. T. Maddalena, Y. Jiang and C. N. Jones
%
% Estimating RKHS complexity from data

%%
clear all
close all
rng(20)
figure

f = groundTruth();
x = linspace(f.xmin,f.xmax,200);

X = []; nrm = [];
n = 80;

for i = 1:n
    
    % sampling and estimating the RKHS norm
    xnew = rand(1,1).*(f.xmax-f.xmin) + f.xmin;
    X    = [X; xnew];
    fX   = f.eval(X')';
    K    = f.kernel(X,X') + 1e-9*eye(i);
    nrm  = [nrm sqrt(fX'*(K\fX))];
    
    % plotting
    if mod(i,10) == 0
        clf
        subplot(2,1,1)
        plot(x,f.eval(x),'linewidth',2); hold on
        set(gcf,'color','w'); set(gca,'FontWeight','normal','FontSize',16,'FontName','Helvetica');
        scatter(X,fX,60,'filled','r'); grid on
        subplot(2,1,2)
        plot(0:i,[0 nrm],'linewidth',2); hold on
        plot(0:n,f.rkhs_norm*ones(1,n+1),'k--','linewidth',2)
        set(gcf,'color','w'); set(gca,'FontWeight','normal','FontSize',16,'FontName','Helvetica');
        axis([0 n 0 1.1*f.rkhs_norm]); grid on
        pause
    end
    
end

% EOF