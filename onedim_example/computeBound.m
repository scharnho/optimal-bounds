% Supplementary material for the paper:
% Robust Uncertainty Bounds in Reproducing Kernel Hilbert Spaces:  
% A Convex Optimization Approach'
% Authors: P. Scharnhorst, E. T. Maddalena, Y. Jiang and C. N. Jones
%
% A one-dimensional example

%% Solves the primal problem
function BND = computeBound(x, data, kernel, gamma, delBar, dir)

    if strcmp(dir,'up'), disp('Computing upper bound...'); elseif strcmp(dir,'dw'), disp('Computing lower bound...'); end

    X = data(:,1);
    y = data(:,2);
    N = numel(X);

    K = kernel(X,X) + 0.000000001*eye(N); 

    BND = []; idx = 0;
    for z = x
        
        if mod(idx,10) == 0, fprintf(['Progress: ' num2str(idx) '/' num2str(numel(x)) '\n']); end

        c  = sdpvar(N,1);
        cz = sdpvar(1);

        % upper or lower bound?
        if strcmp(dir,'up'), obj = -cz; elseif strcmp(dir,'dw'), obj = cz; end
            
        const = [[c; cz]'*([K kernel(X,z); kernel(X,z)' kernel(z,z)]\[c; cz]) <= gamma^2];
        const = [const, norm(c - y, inf) <= delBar];

        options = sdpsettings('verbose', 0, 'solver', 'mosek');
        sol = optimize(const,obj,options);

        BND = [BND value(cz)];
        
        idx = idx + 1;

    end
    
    fprintf(['Progress: ' num2str(idx) '/' num2str(numel(x)) '\n\n'])
    
end

%EOF