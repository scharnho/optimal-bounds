% Supplementary material for the paper:
% Robust Uncertainty Bounds in Reproducing Kernel Hilbert Spaces:  
% A Convex Optimization Approach'
% Authors: P. Scharnhorst, E. T. Maddalena, Y. Jiang and C. N. Jones
%
% A one-dimensional example

%%
clear all
close all
rng(1); 
clc

% the ground-truth: a simple sine wave
f.foo  = @(x) sin(x);
f.xmin = -pi; 
f.xmax =  pi;

% defining the kernel
l = 2; 
kernel = @(x1,x2) exp(-dist(x1,x2').^2 / (2*l^2));

% estimating the RKHS norm
gamma = normEstimator(f, kernel);

% collecting data
N      = 40; 
delBar = 0.1;

X    = rand(N,1).*(f.xmax-f.xmin) + f.xmin;
fX   = f.foo(X);
del  = (rand(N,1)*2*delBar) - delBar;
y    = fX + del;
data = [X y];

% calculating bounds
x = linspace(f.xmin, f.xmax, 100);
UB = computeBound(x, data, kernel, gamma, delBar, 'up');
LB = computeBound(x, data, kernel, gamma, delBar, 'dw');

% plotting
color = '#0072BD';
plot(x,UB,'Color',color,'linewidth',2); hold on
plot(x,LB,'Color',color,'linewidth',2)
scatter(X,y,40,'MarkerEdgeColor','k','MarkerFaceColor',color);
env = patch([x fliplr(x)], [LB fliplr(UB)], 'k');
env.FaceColor = color; env.FaceAlpha = 0.1;

xlabel('$x$','Interpreter','latex'); ylabel('$f(x)$','Interpreter','latex'); 
title('Deterministic uncertainty envelope');
set(gca,'FontWeight','normal','FontSize',16,'FontName','Helvetica');
grid on; set(gcf,'color','w');

% EOF